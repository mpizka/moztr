#! /usr/bin/env python3

# read URLs from open firefox tabs and print them to command line

import lz4
import json
from sys import *

if len(argv) < 2: stderr.write('Missing file argument\n')

try:
    fp = open(argv[1], 'rb')
except Exception as err:
    stderr.write('cannot open file: ' + str(err) + '\n')
    exit()

# FF adds 8 bytes to the beginning of the LZ4 file which we need to discard
stdout.write(str(fp.read(8)))
content = lz4.decompress(fp.read())
stdout.write(str(content))
