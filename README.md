# moztr

Mozilla Firefox Tab Reader v2

Command line tool reading mozilla firefox tabs from the command line.

This is version 2 of the implementation, since MIzilla decided to compress the files using the LZ4 compression library.

# license

This project is under a modified MIT license. Find additional information in license.txt

# version

0.0.1 Development, not fit for anything